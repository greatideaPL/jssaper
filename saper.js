var images = {
    mineURL: 'url("http://www.sireasgallery.com/iconset/minesweeper/Mine_256x256_32.png")'
    , flagURL: 'url("http://www.gideonsgolf.ca/images/icon-flag.png")'
}
var gameStatistics = {
    mineLeft: 0
    , placeToCheck: 0
    , gameReset: function () {
        this.mineLeft = 0;
        this.placeToCheck = 0;
    }
    , mineLeftChange: function (step) {
        this.mineLeft += step;
    }
    , placeToCheckChange: function (step) {
        this.placeToCheck += step;
    }
}
var myGameTimeInterval = setInterval(function () {
    input = $('.playTime').html();
    $('.playTime').html(++input);
}, 1000);

function getDifficult() {
    return $('#difficult').val();
};

function offButtonEvents() {
    $('body').off('click', '.btn');
    $('body').off('contextmenu', '.btn');
}

function makeInput() {
    $('#gameStatistics').html('');
    $('#gameStatistics').append($('<div class="info">Casz rozminowywania: </div><div class="playTime value">0</div>'));
    $('#gameStatistics').append($('<div class="info">Pól do pszeszukania: </div><div class="areaLeft value"></div>'));
    $('#gameStatistics').append($('<div class="info">Min do oznaczenia: </div><div class="mineLeft value"></div>'));
    setGameNewStatictics();
}

function setGameNewStatictics() {
    $('.areaLeft').html(gameStatistics.placeToCheck);
    $('.mineLeft').html(gameStatistics.mineLeft);
}


function goodPlayCheck() {
    var value = 0;
    $('.btn').each(function () {
        if (($(this).data('value') === 'B') && ($(this).css('backgroundImage') === images.flagURL) || (($(this).data('value') != 'B') && ($(this).css('backgroundImage') != images.flagURL))) {
            value++;
        }
    });
    return value;
}

function _makeBoard(size, diff) {
    let _board = [];
    gameStatistics.gameReset();
    gameStatistics.placeToCheck = size * size;
    for (let i = 0; i < size; i++) {
        let _row = [];
        for (let j = 0; j < size; j++) {
            //    _row[j] = Math.random() * 10 < diff ? "B" : 0;
            if (Math.random() * 10 < diff) {
                _row[j] = "B";
                gameStatistics.mineLeft++;
            }
            else {
                _row[j] = 0;
            }
        }
        _board[i] = _row;
    }
    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            if (typeof (_board[i][j]) === "number") {
                for (let m = -1; m < 2; m++) {
                    for (let n = -1; n < 2; n++) {
                        if ((i + n) >= 0 && (j + m) >= 0 && (i + n) < size && (j + m) < size) { //ominięcie sprawdzania poza krawędzią
                            if (_board[i + n][j + m] === "B") {
                                _board[i][j]++;
                            }
                        }
                    }
                }
            }
        }
    }
    return _board;
}

function _printBoard(_tablica, _callback) {
    let _partial = "<table border=0 cellpadding=0 cellspacing=0>";
    for (let i = 0; i < _tablica.length; i++) {
        _partial += "<tr>";
        for (let j = 0; j < _tablica[i].length; j++) {
            _partial += _makeButton(i, j, _tablica[i][j]);
        }
        _partial += "</tr>";
    }
    _partial += "</table>";
    document.getElementById('plansza').innerHTML = _partial;
    setTimeout(function () {
        _callback();
    }, 0);
}

function _makeButton(x, y, _value) {
    return `<td>
     	<button 
      	class='btn'
        data-x='` + x + `'` + `data-y='` + y + `'` + `data-value=` + _value + `> `
        /*+ array[x][y] */
        + `</button></td>`;
}
var bindEvents = function () {
    $('.btn').on('click', function () {
        //Jeśli jest bomba
        if ($(this).data('value') === 'B') {
            showAllmap();
            //clearInterval(myGameTimeInterval);
            alert("Zdetonowałeś minę. Koniec gry");
            $("table").toggle("explode", {pieces: 36}, 1500);
            //offButtonEvents();
        }
        // Jeśli nie ma bomby
        else if ($(this).css('backgroundImage') != images.flagURL) {
            $(this).html(($(this).data('value')));
            $(this).addClass('safeArea')
            gameStatistics.placeToCheckChange(-1);
        }
        setGameNewStatictics();
        endGameCheck();
    });
    $('.btn').contextmenu(function (event) {
        event.preventDefault();
        // Jeśli nieoznaczone pole:
        if ($(this).css('backgroundImage') != images.flagURL) {
            $(this).css({
                'backgroundImage': images.flagURL
            });
            $(this).html('');
            gameStatistics.placeToCheckChange(-1);
            gameStatistics.mineLeftChange(-1);
        }
        // Jeśli oflagowane pole
        else {
            $(this).css({
                'backgroundImage': ''
            })
            gameStatistics.placeToCheckChange(1);
            gameStatistics.mineLeftChange(1);
        }
        setGameNewStatictics();
        endGameCheck();
    });
}
var setArraySize = function () {
    $('#btnNgame').on('click', function () {
        var arraySize = $('#arrSize').val();
        var difficult = getDifficult();
        gameStatistics.placeToCheck = arraySize * arraySize;
        _printBoard(_makeBoard(arraySize, difficult), bindEvents);
        makeInput();
        myGameTimeInterval;
    });
}
var showAllmap = function () {
        let _buttons = document.getElementsByClassName('btn');
        for (let i = 0; i < _buttons.length; i++) {
            if (_buttons[i].dataset.value === "B") {
                _buttons[i].style.backgroundImage = images.mineURL;
            }
            else _buttons[i].firstChild.data = _buttons[i].dataset.value;
        }
    }
function getScore(){
    var arraysize = $('#arrSize').val();
    var playTime = $('.playTime').html()
    var score = Math.floor(10 * arraysize * arraysize * arraysize * getDifficult()/ playTime)
    return score;
    
}
function endGameCheck() {
    if (gameStatistics.placeToCheck === 0) {
        let arrSize = $('#arrSize').val()
        if (goodPlayCheck() === (arrSize * arrSize)) {
            alert("Koniec gry, Zdobyłeś " + getScore() + " punktów!");
        }
        else {
            alert("Źle oznaczyłeś pola");
           // showAllmap();
        }
        
    }
    return false;
}
    //Old bindEvents() function without JQUERRY
    /*var bindEventsOld = function () {
        let _buttons = document.getElementsByClassName('btn');
        for (let i = 0; i < _buttons.length; i++) {
            _buttons[i].onclick = function (event) {
                //Jeśli jest bomba
                if (this.dataset.value === "B") {
                    showAllmap();
                    clearInterval(myGameTimeInterval);
                    offButtonEvents();
                    alert("Zdetonowałeś minę. Koniec gry");
                }
                // Jeśli nie ma bomby
                else if (this.style.backgroundImage === "") {
                    this.firstChild.data = this.dataset.value;
                    gameStatistics.placeToCheckChange(-1);
                }
                setGameNewStatictics();
                endGameCheck();
            };
            _buttons[i].oncontextmenu = function (event) {
                event.preventDefault();
                // Jeśli nieoznaczone pole:
                if (this.style.backgroundImage === "") {
                    this.style.backgroundImage = images.flagURL;
                    this.firstChild.data = "";
                    gameStatistics.placeToCheckChange(-1);
                    gameStatistics.mineLeftChange(-1);
                }
                // Jeśli oflagowane pole
                else {
                    this.style.backgroundImage = "";
                    gameStatistics.placeToCheckChange(1);
                    gameStatistics.mineLeftChange(1);
                }
                setGameNewStatictics();
                endGameCheck();
            }
        }
    }
     */
setArraySize();